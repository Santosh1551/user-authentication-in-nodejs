const express = require("express");
const app = express();
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const postRoute = require('./routes/posts');


// Import Routes
const authRoute = require('./routes/auth');

dotenv.config();

// Connect to DB
mongoose.connect(process.env.DB_CONNECT, 
    { useNewUrlParser: true, useUnifiedTopology: true},
    () => console.log('connected to db')
);

// Middleware
app.use(express.json());

// Routes Middleware
app.use('/api/user', authRoute);
app.use('/api/posts', postRoute);

app.listen(5000, () => {console.log("server started at port 5000")}); 